# lanthanum

<https://blue-webapps.gitlab.io/lanthanum>

## quick start for react webapps

react webpack eslint webapp

## create a project

```bash
# download the lanthanum skeleton code
wget https://gitlab.com/blue-webapps/lanthanum/repository/archive.zip?ref=master -O /tmp/lanthanum.zip

# enter your projects directory
cd ~/projects

# unzip lanthanum into your project
unzip /tmp/lanthanum.zip

# rename the project
mv lanthanum-master mywebapp

# edit package.json to replace lanthanum with myapp
cd mywebapp
nano package.json

# install dependencies
npm i
```

## live development

in this mode the webapp is served by webpack-dev-server, when you make a change your browser will automatically reload to show the change

```bash
cd ~/projects/mywebapp
npm run start
```

open <http://localhost:8080/> in your browser

## build release

in this mode webpack is used to bundle your webapp into the dist directory ready to be deployed to your server, it is uglified and minified with sourcemaps, if your webapp works offline you can zip the dist folder and send it to someone and they can unzip and run your app by opening index.html

```bash
cd ~/projects/mywebapp
npm run build
```
