const USERS_URL = 'https://jsonplaceholder.typicode.com/users';

const list = () =>
  fetch(USERS_URL).then((res) => {
    if (!res.ok) {
      throw new Error('Error getting users');
    } else {
      return res.json().then((json) => json.map(({ id, name, email }) => ({ id, name, email })));
    }
  });

export default { list };
