import React from 'react';

const fontStyle = {
  fontFamily: 'Ubuntu Mono, monospace',
  fontWeight: '400',
  fontSize: '1.4rem',
  color: 'blue',
  textShadow: '2px 2px 5px cyan',
  margin: 0,
};

const App = () => (
  <div
    style={{
      display: 'flex',
    }}
  >
    <div style={{ flex: 1 }} />

    <div style={{ padding: '2rem', maxWidth: '1000px' }}>
      <h1
        style={{
          ...fontStyle,
          fontSize: '3rem',
          textAlign: 'center',
          paddingBottom: '2rem',
        }}
      >
        lanthanum
      </h1>

      <div style={{ paddingBottom: '2rem' }} />
    </div>

    <div style={{ flex: 1 }} />
  </div>
);

export default App;
