/* eslint-disable import/no-nodejs-modules */
/* eslint no-console: off */

import assert from 'assert';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
// eslint-disable-next-line import/default
import CopyWebpackPlugin from 'copy-webpack-plugin';
import { readFileSync } from 'fs';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { resolve } from 'path';
import process from 'process';
import webpack from 'webpack';

const { title } = JSON.parse(readFileSync('./package.json'));
assert(title, 'No title specified in package.json');

const { BASE_PATH } = process.env;

const config = {
  entry: {
    index: resolve('./src/index.jsx'),
  },
  output: {
    filename: '[name].[contenthash].js',
    path: resolve('./public'),
  },
  optimization: {
    moduleIds: 'deterministic',
    runtimeChunk: 'single',
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: 'vendors',
          chunks: 'all',
        },
      },
    },
  },
  plugins: [
    new webpack.DefinePlugin({ BASE_PATH: JSON.stringify(BASE_PATH) }),

    new CleanWebpackPlugin(),

    new CopyWebpackPlugin({
      patterns: [{ from: 'assets', to: 'assets' }],
    }),

    new HtmlWebpackPlugin({
      title,
      BASE_PATH: BASE_PATH || '/',
      template: resolve('./src/index.ejs'),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-react', '@babel/preset-env'],
        },
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      '~': resolve('./src/'),
    },
  },
};

export default config;
