const PHOTOS_URL = 'https://jsonplaceholder.typicode.com/photos';

const list = () =>
  fetch(PHOTOS_URL).then((res) => {
    if (!res.ok) {
      throw new Error('Error getting photos');
    } else {
      return res.json();
    }
  });

export default { list };
